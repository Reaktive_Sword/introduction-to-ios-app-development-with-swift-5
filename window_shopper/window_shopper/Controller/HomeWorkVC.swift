import UIKit

class HomeWorkVC: UIViewController {
    @IBOutlet weak var mileTextField: UITextField!
    @IBOutlet weak var kilometrTextField: UITextField!
    @IBOutlet weak var poundTextField: UITextField!
    
    @IBOutlet weak var kilogramTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func calculateFromMtoK(_ sender: Any) {
        if let mileTxt = mileTextField.text{
            if let mile = Double(mileTxt) {
                let answer = mile * 1.60934
                kilometrTextField.text = "\(answer)"
            }
        }
    }
    @IBAction func calculateFromKtoM(_ sender: Any) {
        if let kmTxt = mileTextField.text{
            if let km = Double(kmTxt) {
                let answer = km / 1.60934
                mileTextField.text = "\(answer)"
            }
        }
    }
    @IBAction func calcPoundToKilogram(_ sender: Any) {
        if let pnTxt = poundTextField.text{
            if let pn = Double(pnTxt) {
                let answer = pn * 0.453592
                kilogramTextField.text = "\(answer)"
            }
        }
    }
    @IBAction func calcKilogramToPound(_ sender: Any) {
        if let kgTxt = kilogramTextField.text{
            if let kg = Double(kgTxt) {
                let answer = kg / 0.453592
                poundTextField.text = "\(answer)"
            }
        }
    }
    
}
