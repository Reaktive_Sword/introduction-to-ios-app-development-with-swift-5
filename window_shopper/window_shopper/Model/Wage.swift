//
//  Wage.swift
//  window_shopper
//
//  Created by Булат Хасанов on 22.10.2023.
//

import Foundation
class Wage{
    class func getHours(forWage wage: Double, andPrice price: Double) -> Int {
        return Int(ceil(price / wage))
    }
}
